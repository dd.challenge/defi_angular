# Mock server - Tweets @DesjardinsGroup, @DesjardinsLab, @DesjardinsCoop

## About

This is a mocker server set-up that runs on [json-server](https://github.com/typicode/json-server).  There are instructions on this repo on how to set-it up locally.

Ensure you have Node and npm, and you can run the command to install it globally on your computer:
```bash
npm install -g json-server
```

The purpose of this mock-server is to ensure you can focus on the front-end, as this server will return JSON responses on GET requests of their respesctive endpoints.

## Setting up

Once you have Node, npm, and json-server installed, go to the root of the server package (where package.json is located), and run:
```bash
npm install
```
This command will install the dependencies need to run the mock-server.

To get the server going, run:
```bash
npm run server
```

You will see the server up and running on [http://localhost:3000/](http://localhost:3000/)

## How it works

The **db.json** file is where the data is located when you make your GET requests.  

The available requests are:

 - [http://localhost:3000/desjardinsgroup](http://localhost:3000/desjardinsgroup)
 - [http://localhost:3000/desjardinslab](http://localhost:3000/desjardinslab)
 - [http://localhost:3000/desjardinscoop](http://localhost:3000/desjardinscoop)

These requests return an array of tweets, which you will have to work with in the challenge.

For any questions, do not hesitate to get in touch with us!