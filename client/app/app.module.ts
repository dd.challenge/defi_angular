import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routes } from './app.routing';
import { HttpClientModule } from '@angular/common/http';
import { DragulaModule } from 'ng2-dragula';

// Main app component
import { AppComponent } from './app.component';

// Components
import { SettingsComponent } from './components/settings.component';
import { HomeComponent } from './components/home.component';
import { HeaderComponent } from './components/header.component';
import { TweetComponent } from './components/tweet.component';
import { TweetColumnComponent } from './components/tweet-column.component';

// Services
import { TwitterAPIService } from './services/twitter-api.service';
import { SettingsService } from './services/settings.service';
import { DragulaService } from 'ng2-dragula/ng2-dragula';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SettingsComponent,
    HeaderComponent,
    TweetComponent,
    TweetColumnComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    DragulaModule,
    routes
  ],
  providers: [
    TwitterAPIService,
    SettingsService,
    DragulaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
