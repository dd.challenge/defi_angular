import { Routes, RouterModule } from '@angular/router';
import { SettingsComponent } from './components/settings.component';
import { HomeComponent } from './components/home.component';

const APP_ROUTES: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full'},
    { path: 'home', component: HomeComponent },
    { path: 'settings', component: SettingsComponent },
    { path: '**', redirectTo: 'home' }
]

export const routes = RouterModule.forRoot(APP_ROUTES);