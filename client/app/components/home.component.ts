import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { HttpErrorResponse } from '@angular/common/http';

// Services
import { SettingsService } from '../services/settings.service';
import { TwitterAPIService } from '../services/twitter-api.service';

// Models
import { TweetModel, SettingsModel } from '../models/_index';

@Component({
    selector: 'app-home',
    template: ` 
        <app-tweet-column class="col-xs-6 col-md-4 " 
            *ngFor="let cleanTweetArray of tweetRequestsAllUsersParallel$" 
            [twitterCollectionReceived]="cleanTweetArray"
            [colorTheme]="colorTheme"
        >
        </app-tweet-column>
    `
})
export class HomeComponent implements OnDestroy {

    tweetRequestsAllUsersParallel$: TweetModel[][];
    colorTheme: string;
    colorThemeSubscription: Subscription;

    constructor(private _twitterAPIService: TwitterAPIService,
        private _settingsService: SettingsService) {

        // Subscribing the the API Get query from Twitter
        this._twitterAPIService.refreshTweets().subscribe(
            data => {
                this.tweetRequestsAllUsersParallel$ = this._twitterAPIService.cleanTwitterCollections(JSON.parse(JSON.stringify(data)));
            },
            (err: HttpErrorResponse) => {
                if (err.error instanceof Error) {
                    //Client side or network error
                    console.error('Error occured:', err.error.message);
                } else {
                    //Backend error
                    console.error(`Backend returned code ${err.status}, body was: ${JSON.stringify(err.error)}`);
                }
            });

        // Listening to the change in application theme from the Settings page to apply to the Home view and its child components
        this.colorThemeSubscription = this._settingsService.applicationSettingsChanged$.subscribe(
            (applicationSettings: SettingsModel) => { this.colorTheme = applicationSettings.colorTheme }
        );
    }

    // We unsubscribe in components to prevent memory leaks for lost instances of this component
    ngOnDestroy(): void {
        if (this.colorThemeSubscription) {
            this.colorThemeSubscription.unsubscribe();
        }
    }

}