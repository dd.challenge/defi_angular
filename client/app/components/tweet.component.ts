import { Component, Input } from '@angular/core';
import { TweetModel } from '../models/_index';

@Component({
  selector: 'app-tweet',
  template: `
    <div class="single-tweet-container">
      <p>{{ receivedTweet.tweetContent}}</p>
      <p>{{ receivedTweet.date | date:'medium' }}</p>
      <p><a title="{{ tweetLink }}" href="{{ tweetLink }}" target="_blank">Link to tweet</a></p>
      <p><a title="Go to user profile on Twitter - [{{ userTwitterProfileLink }}]" href="{{ userTwitterProfileLink }}" target="_blank">@{{ receivedTweet.user }}</a></p>
    </div>
  `,
  styles: [`
    .single-tweet-container {
      background-color: aliceblue;
      overflow: hidden;
      padding: 15px;
      border: 1px solid rgba(55, 55, 55, .2);
      margin: 15px 10px;
    }
  `]
})
export class TweetComponent {
  receivedTweet: TweetModel;
  tweetLink: string;
  userTwitterProfileLink: string;

  @Input()
  set processReceivedTweet(receivedTweet: TweetModel) {
    this.receivedTweet = receivedTweet;
    this.tweetLink = receivedTweet.link;
    this.userTwitterProfileLink = 'https://twitter.com/' + receivedTweet.user;
  }
}
