import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

// Services
import { SettingsService } from '../services/settings.service';

// Models
import { SettingsModel } from '../models/_index';

@Component({
    selector: 'app-header',
    template: `
        <nav class="row" [style.backgroundColor]="colorTheme">
            <div class="col-xs-4 col-md-4">
                <button class="btn btn-default navbar-btn" (click)="goHome()"><i class="glyphicon glyphicon-home"></i>Home</button>
            </div>
            <div class="col-xs-8 col-md-8 settings-button-container">
                <button class="btn btn-default navbar-btn" (click)="goSettings()"><i class="glyphicon glyphicon-cog"></i>Settings</button>
            </div>
        </nav>
    `,
    styles: [`
        nav {
            padding: 10px 15px;
            margin: 0px 15px;
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            border-bottom: 1px solid rgba(55, 55, 55, .3);
        }

        i {
            margin-right: 10px;
        }

        .settings-button-container {
            text-align: right;
        }
    `]
})
export class HeaderComponent implements OnDestroy {

    colorTheme: string;
    colorThemeSubscription: Subscription;

    constructor(private _router: Router,
        private _settingsService: SettingsService) {
        this.colorThemeSubscription = this._settingsService.applicationSettingsChanged$.subscribe(
            (applicationSettings: SettingsModel) => { this.colorTheme = applicationSettings.colorTheme }
        );
    }

    ngOnDestroy(): void {
        this.colorThemeSubscription.unsubscribe();
    }

    goHome(): void {
        this._router.navigate(['/', 'home']);
    }

    goSettings(): void {
        this._router.navigate(['/', 'settings']);
    }

}