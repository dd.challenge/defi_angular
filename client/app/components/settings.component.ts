import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { DragulaService } from 'ng2-dragula/ng2-dragula';

// Services
import { SettingsService } from '../services/settings.service';

// Models
import { SettingsModel } from '../models/settings.model';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnDestroy {

    applicationSettings: SettingsModel;
    private applicationSettingsSubscription: Subscription;

    usersArray: Array<string>;

    private dragulaSubscription: Subscription;

    constructor(private _settingsService: SettingsService,
        private _dragulaService: DragulaService) {

        // Listening to changes in the application settings model state
        this.applicationSettingsSubscription = this._settingsService.applicationSettingsChanged$.subscribe(
            (applicationSettings: SettingsModel) => {
                this.applicationSettings = applicationSettings;
                this.usersArray = applicationSettings.usersArray;
            }
        );

        // Dragula plugin listening to changes in the column order and setting the result in the application settings model state
        this.dragulaSubscription = this._dragulaService.dropModel.subscribe((value) => {
            this.applicationSettings.usersArray = this._dragulaService.find('first-bag').drake.models[0];
            this._settingsService.applicationSettings.next(this.applicationSettings);
        })
    }

    ngOnDestroy(): void {
        if (this.applicationSettingsSubscription) {
            this.applicationSettingsSubscription.unsubscribe();
        }

        if (this.dragulaSubscription) {
            this.dragulaSubscription.unsubscribe();
        }
    }

    applyTheme(theme: string): void {
        this.applicationSettings.colorTheme = theme;
        this._settingsService.applicationSettings.next(this.applicationSettings);
    }

}