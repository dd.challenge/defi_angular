import { Component, Input } from '@angular/core';
import { TweetComponent } from './tweet.component';
import { TweetModel } from '../models/tweet.model';

@Component({
    selector: 'app-tweet-column',
    template: `
        <div class="collection-container" [style.backgroundColor]="colorTheme">
            <h2><span class="labels">User:</span> @{{ twitterUser }}</h2>
            <h3><span class="labels">Number of Tweets:</span> {{ twitterCollectionLength }}</h3>
            <p>
                <span class="labels">Tweet dates range:</span>
                <span class="dateRanges">{{ endDate | date:'short' }}</span>
                <span class="labels">to</span> 
                <span class="dateRanges"> {{ startDate | date:'short'}}</span>
            </p>
            <app-tweet *ngFor="let singleTweet of twitterCollection" [processReceivedTweet]="singleTweet"></app-tweet>
        </div>
    `,
    styles: [`
        .collection-container {
            padding: 10px;
            margin: 15px 0;
            border: 1px solid rgba(55, 55, 55, .5);
            overflow: hidden;
        }

        .labels {
            font-weight: bold;
        }

        .dateRanges {
            display: block;
        }

        h2, h3, p {
            text-align: center;
        }
    `]
})
export class TweetColumnComponent {

    twitterCollection: TweetModel[];
    twitterUser: string;
    twitterCollectionLength: number;
    startDate: Date;
    endDate: Date;

    @Input() colorTheme: string;
    @Input()
    set twitterCollectionReceived(twitterCollection: TweetModel[]) {
        this.twitterCollection = twitterCollection;
        this.twitterUser = twitterCollection[0].user;
        this.twitterCollectionLength = twitterCollection.length;
        this.endDate = twitterCollection[0].date;
        this.startDate = twitterCollection[twitterCollection.length - 1].date;
    }

}