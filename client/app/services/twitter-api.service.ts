import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/forkJoin';

// Services
import { SettingsService } from './settings.service';

// Models
import { TweetModel, UserTweetCollections, SettingsModel } from '../models/_index';

@Injectable()
export class TwitterAPIService {

    private users: Array<string>;
    private numberOfTweetsPerUser: number;

    numberOfTweetsPerUserSubscription: Subscription;
    tweetRequestsAllUsersParallel$: Observable<Object>;

    constructor(private _httpClient: HttpClient,
        private _settingsService: SettingsService) {

        // Listening to settings values which make the Twitter API call - which users and the number of tweets per user
        this.numberOfTweetsPerUserSubscription = this._settingsService.applicationSettingsChanged$.subscribe(
            (applicationSettings: SettingsModel) => {
                this.users = applicationSettings.usersArray;
            }
        )
    }

    refreshTweets(): Observable<Object> {
        let tweetCollectionsPerUser = [];
        for (let index = 0, length = this.users.length; index < length; index++) {
            tweetCollectionsPerUser.push(this.getTweetCollectionByUser(this.numberOfTweetsPerUser, this.users[index]));
        }

        // forkJoin allows us to group together the GET requests which are Observables and will emit the last value from each when they complete
        return Observable.forkJoin(tweetCollectionsPerUser);
    }

    getTweetCollectionByUser(numberOfTweetsPerUser: number, userName: string): Observable<Object> {
        return this._httpClient.get(`http://localhost:3000/${userName}`);
    }

    cleanTwitterCollections(twitterCollection: Array<Object>): TweetModel[][] {
        let createCleanTweetCollection = (tweetCollection) => {
            let filteredArrayOfTweets = [];

            //This loops is used to convert the received API Object to an array to be able to map an extract the values we want per TweetModel
            for (let tweetObject in tweetCollection) {
                let tweetObjectToFilter = tweetCollection[tweetObject];

                // A small check as some Tweets do not have urls - ideally a safe check would be done for all properties extracted
                let existingLink: string = tweetObjectToFilter.entities.urls[0] == null ? '' : tweetObjectToFilter.entities.urls[0].url;
                filteredArrayOfTweets.push(<TweetModel>{
                    tweetContent: tweetObjectToFilter.text,
                    date: tweetObjectToFilter.created_at,
                    link: existingLink,
                    user: tweetObjectToFilter.user.screen_name
                });
            }
            return filteredArrayOfTweets;
        }
        return twitterCollection.map(createCleanTweetCollection);
    }
}