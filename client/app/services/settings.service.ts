import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { SettingsModel } from '../models/_index';

@Injectable()
export class SettingsService {

    // BehaviorSubject is an Observable that takes a value by default - in this case an initial settings state
    applicationSettings: BehaviorSubject<SettingsModel> = new BehaviorSubject<SettingsModel>({
        colorTheme: 'white',
        usersArray: ['desjardinsgroup', 'desjardinslab', 'desjardinscoop']
    });
    applicationSettingsChanged$: Observable<SettingsModel> = this.applicationSettings.asObservable();
    applicationSettingsSubscription: Subscription;

    constructor() {
        // On first app init we check the localStorage for the settings 
        this.getLocalStorage();

        // Whenever a change occurs in the settings state of the app we update the localStorage
        this.applicationSettingsSubscription = this.applicationSettingsChanged$.subscribe(
            (applicationSettings: SettingsModel) => {
                this.setLocalStorage(applicationSettings);
            }
        );
    }

    setLocalStorage(applicationSettings): void {
        localStorage.setItem('settings', JSON.stringify(applicationSettings));
    }

    getLocalStorage(): void {
        if (localStorage.getItem('settings') !== null) {
            this.applicationSettings.next(JSON.parse(localStorage.getItem('settings')));
        }
    }

}