export interface TweetModel {
    tweetContent?: string;
    date?: Date;
    link?: string;
    user?: string;
}