export interface SettingsModel {
    colorTheme?: string;
    usersArray?: Array<string>;
}