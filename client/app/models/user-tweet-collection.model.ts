import { TweetModel } from './tweet.model';

export interface UserTweetCollections {
    user?: string;
    tweetCollection?: Array<TweetModel>
}