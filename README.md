# Front-end Developer Challenge - Angular2 Solution

# Information: 
  
This is a solution for the desjardins Front-end developer Challenge using Angular 2.  The server folder is what is sent to each applicant, and is integrated in this repo.

# Setup Information

Ensure you have Node and npm install, as well as the [Angular-CLI](https://cli.angular.io/)
  
**Step 1:**     
  
Run `npm install` at the project root to grab all necessary dependencies for the client-side project.
  
## Running a Development Server  
  
Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

It's important that the mock-server is up and running for the HTTP requests.
  
## Building a Production Version  
  
Run `npm run build-prod` to build the project. The build artifacts will be stored in the `dist/` directory.    